using System;
using UnityEngine;
using UnityEngine.UI;

public enum ButtonsTypes
{
    Play,
    Dificulty,
    Exit
}

public enum Difficulty
{
    Normal,
    HardCore
}

public class MainUIController : MonoBehaviour
{
    public event Action<ButtonsTypes,Difficulty?> PressButton;

    [SerializeField] private Button playButton;
    [SerializeField] private Button dificultyButton;
    [SerializeField] private Button exitButton;

    private Difficulty dificulty = Difficulty.Normal;

    private Canvas canvas;

    private Text dificultyTextButton;

    private const string hardCoreText = "Dificultad:Dif�cil";
    private const string normalText = "Dificultad:Normal";

    private void Awake()
    {
        canvas = GetComponent<Canvas>();

        dificultyTextButton = dificultyButton.GetComponentInChildren<Text>();

        playButton.onClick.AddListener(OnPressPlay);
        dificultyButton.onClick.AddListener(OnPressDificulty);
        exitButton.onClick.AddListener(OnPressExit);
    }

    public void OnPressPlay()
    {
        PressButton?.Invoke(ButtonsTypes.Play,null);
    }

    public void OnPressDificulty()
    {
        dificulty = dificulty.Equals(Difficulty.Normal) ? Difficulty.HardCore : Difficulty.Normal;

        dificultyTextButton.text = string.Format("{0}",dificulty.Equals(Difficulty.Normal) ? normalText : hardCoreText);

        PressButton?.Invoke(ButtonsTypes.Dificulty,dificulty);
    }

    public void OnPressExit()
    {
        PressButton?.Invoke(ButtonsTypes.Exit, null);
    }

    public void SetEnable(bool enabled)
    {
        canvas.enabled = enabled;
    }
}
