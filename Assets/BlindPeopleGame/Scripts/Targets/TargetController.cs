using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField] Difficulty currentDifficulty;

    private Sprite sprite;

    private void Awake()
    {
        //sprite = GetComponentInChildren<Sprite>();
    }

    public void Enabled(BodyPart bodyPartToTarget)
    {

    }

    public void SetDificulty(Difficulty difficulty)
    {
        currentDifficulty = difficulty;

        switch (difficulty)
        {
            case Difficulty.Normal:
                transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                break;
            case Difficulty.HardCore:
                transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                break;
            default:
                break;
        }
    }

    public void Disable()
    {
        Destroy(gameObject);
    }
}
