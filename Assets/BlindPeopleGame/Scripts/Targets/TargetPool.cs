using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TargetPool : MonoBehaviour
{
    [SerializeField] private SpawnOptions bodyPartOptionMouseOption;
    [SerializeField] private GameObject targetPrefab;
    public TargetController SpawnTarget(BodyPart bodyPart, Difficulty difficulty)
    {
        SpawnOptions selectedSpawnOption = new SpawnOptions();

        switch (bodyPart)
        {
            case BodyPart.LeftHand:
                break;
            case BodyPart.RightHand:
                break;
            case BodyPart.Mouse:
                selectedSpawnOption = bodyPartOptionMouseOption;
                break;
            default:
                break;
        }

        GameObject spawnedObject = Instantiate(targetPrefab, transform);

        Vector3 posToMove = new Vector3(GetRandomPos(selectedSpawnOption.minPosToSpawn.x, selectedSpawnOption.maxPosToSpawn.x),
           GetRandomPos(selectedSpawnOption.minPosToSpawn.y, selectedSpawnOption.maxPosToSpawn.y), 0);

        spawnedObject.transform.position = posToMove;
        TargetController target = spawnedObject.GetComponent<TargetController>();
        target.SetDificulty(difficulty);

        return target;
    }

    private float GetRandomPos(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }
}

[Serializable]
public class SpawnOptions
{
    public Vector3 minPosToSpawn;
    public Vector3 maxPosToSpawn;
}
