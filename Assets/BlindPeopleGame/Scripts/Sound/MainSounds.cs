using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSounds : MonoBehaviour
{
    [SerializeField] private AudioClip playClip;
    [SerializeField] private AudioClip toHardClip;
    [SerializeField] private AudioClip toNormalClip;
    [SerializeField] private AudioClip toExitClip;

    private AudioSource audioSource;

    private Difficulty difficulty = Difficulty.Normal;

    private Coroutine SoundsCoroutine;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }    

    public void SetDifficulty(Difficulty difficulty)
    {
        this.difficulty = difficulty;
    }

    public void SetEnabled(bool enabled)
    {
        if(enabled)
        {
            SoundsCoroutine = StartCoroutine(PlaySounds());
        }
        else
        {
            StopCoroutine(SoundsCoroutine);
            audioSource.Stop();
        }
    }

    IEnumerator PlaySounds()
    {
        yield return new WaitForSeconds(1);
        audioSource.clip = playClip;
        audioSource.Play();
        yield return new WaitForSeconds(playClip.length + 0.5f);
        audioSource.clip = difficulty.Equals(Difficulty.Normal) ? toHardClip : toNormalClip;
        audioSource.Play();
        yield return new WaitForSeconds(difficulty.Equals(Difficulty.Normal) ? toHardClip.length + 0.5f : toNormalClip.length + 0.5f);
        audioSource.clip = toExitClip;
        audioSource.Play();
        yield return new WaitForSeconds(toExitClip.length + 0.5f);
        yield return new WaitForSeconds(5);
        SoundsCoroutine = StartCoroutine(PlaySounds());
    }

}
