using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorSound : MonoBehaviour
{
    [SerializeField] private AudioSource audio;

    public void OnUpdateDistance(float distance)
    {
        if (LeanTween.isTweening(gameObject))
            return;

        print("Distance" + distance);

        if (distance > 0.8f)
            distance = 0.8f;

        float distanceToPlay = distance - 0.2f;

        if (distance < 0.4f)
            distance = 0.4f;

        LeanTween.delayedCall(gameObject,distance - 0.3f, () =>
        {
            audio.Play();
        });        
    }
}
