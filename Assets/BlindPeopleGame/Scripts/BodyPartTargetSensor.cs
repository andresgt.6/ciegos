using UnityEngine;
using System;

public class BodyPartTargetSensor : MonoBehaviour
{
    public event Action<BodyPart> OnReachTarget;

    public float DistanceToTarget { get; private set; }

    [SerializeField] private BodyPart bodyPart;

    private Transform currentTarget;

    private float distanceToReach;

    //Necesito que busques este objetivo
    public void SetTarget(Transform targetToChase, float distanceToReach)
    {
        this.distanceToReach = distanceToReach;
        currentTarget = targetToChase;
    }

    public void SetPos(Vector3 posToMove)
    {
        print("Mouse est� en" + posToMove);
        transform.position = posToMove;
    }

    public void PressMe()
    {
        if (currentTarget)
            OnReachTarget?.Invoke(bodyPart);
    }

    private void Update()
    {
        if (currentTarget)
        {
            //A que distancia est� del objetivo
            DistanceToTarget = Vector3.Distance(transform.position, currentTarget.position);

            //Ya est� cerca al objetivo?
            //if (DistanceToTarget <= distanceToReach)
            //{
            //    OnReachTarget?.Invoke(bodyPart);
            //}
        }
    }
}
