using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Main components")]
    [SerializeField] private MainUIController mainUI;
    [SerializeField] private MainSounds mainSounds;
    [SerializeField] private TargetPool targetPool;
    [SerializeField] private BodyPartTargetSensor mouseBodyPart;
    [SerializeField] private DetectorSound detector;
    [SerializeField] private AudioSource introAudio;

    [Header("Settings")]
    [SerializeField] private float distanceToComplete = 0.2f;

    [Header("Current state")]
    [SerializeField] private Difficulty currentDifficulty = Difficulty.Normal;

    private AudioSource correctAudio;

    private bool inGameplay = false;

    private TargetController currentTarget;

    private void Start()
    {
        correctAudio = GetComponent<AudioSource>();        

        mainUI.PressButton += OnPressMainButton;

        mainUI.SetEnable(true);
        mainSounds.SetEnabled(true);
    }

    private void OnPressMainButton(ButtonsTypes buttonPressed, Difficulty? difficulty)
    {
        print("Presionado el " + buttonPressed);

        switch (buttonPressed)
        {
            case ButtonsTypes.Play:
                PlayIntroAudio();
                mainUI.SetEnable(false);
                mainSounds.SetEnabled(false);
                break;
            case ButtonsTypes.Dificulty:
                if (difficulty.HasValue)
                {
                    currentDifficulty = difficulty.Value;
                    mainSounds.SetDifficulty(currentDifficulty);
                }
                break;
            case ButtonsTypes.Exit:
                Application.Quit();
                break;
            default:
                break;
        }
    }

    private void PlayIntroAudio()
    {
        introAudio.Play();
        LeanTween.delayedCall(introAudio.clip.length, StartGameplay);
    }

    private void StartGameplay()
    {
        SetTarget(BodyPart.Mouse);
        inGameplay = true;
    }

    private void SetTarget(BodyPart bodyPartToShow)
    {
        currentTarget = targetPool.SpawnTarget(bodyPartToShow, currentDifficulty);
        mouseBodyPart.SetTarget(currentTarget.transform, distanceToComplete);
        mouseBodyPart.OnReachTarget += OnReachTarget;
    }

    private void OnReachTarget(BodyPart obj)
    {
        correctAudio.Play();

        mouseBodyPart.OnReachTarget -= OnReachTarget;
        currentTarget.Disable();
        SetTarget(BodyPart.Mouse);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            mainUI.OnPressPlay();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            mainUI.OnPressDificulty();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            mainUI.OnPressExit();
        }


        if (!inGameplay)
            return;

        Ray castPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
        {            
            mouseBodyPart.SetPos(new Vector3(hit.point.x, hit.point.y, 0));
            detector.OnUpdateDistance(mouseBodyPart.DistanceToTarget);

            if(hit.transform.TryGetComponent(out TargetController target))
            {
                mouseBodyPart.PressMe();
            }
        }
    }
}
